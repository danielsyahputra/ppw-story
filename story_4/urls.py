from django.urls import path

from . import views

app_name = 'story_4'

urlpatterns = [
    path('', views.index, name='index')
]